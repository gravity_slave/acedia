class Product < ApplicationRecord
  validates_presence_of :title, :description, :image_url
  IMG_REGEX = %r{\.(gif|jpg|png)\Z}i
  validates_format_of :image_url,
                      allow_blank: true ,
                      with: IMG_REGEX,
      message: 'must be an URL for GIF, JPG or PNG image'
  validates_numericality_of :price,  greater_than_or_equal_to: 0.01
  validates_uniqueness_of :title

end
