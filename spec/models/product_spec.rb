require 'rails_helper'

RSpec.describe Product, type: :model do
 describe 'product' do
   it { should_not allow_value(-1).for(:price) }
   it { should allow_value(1).for(:price) }
   end


  describe 'image' do
    let(:simple_product) { FactoryGirl.create(:product) }
    it do
      should allow_value('fred.gif', 'mark.jpg','greg.png', 'Fred.Jpg', 'FRED.JPG','http://a:b:c:/x/y/z.gif')
                     .for(:image_url)

    end
    it do
      should_not allow_value('fred.gdoc', 'mark.jupg','greg.p11ng', 'Fred.Jp14g', 'FRED.JPpppppG','http://z.gif.more')
                 .for(:image_url)

    end
  end
  end


