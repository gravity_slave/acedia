require 'rails_helper'

RSpec.describe ProductsController, type: :controller do
  describe "GET /products" do
    before do
      get :index
    end
    it { should respond_with(:success)}
    it {should render_template(:index)}
  end

  describe  "GET /new" do
    before { get :new}

    it { should respond_with(:success) }


  end

  describe  "POST /create", redirect: true do
      let(:test_product) { FactoryGirl.attributes_for(:product) }
      it 'should redirect to product path' do
      post :create, product: test_product
      expect(response).to redirect_to(product_path(assigns[:product]))
      end
      it ' expects to change count' do
        expect{
          post :create, product: test_product

        }.to change( Product, :count).by(1)
    end
  end

  describe " PUT /update" do
    let(:awesome_product) { FactoryGirl.create(:product)}
    context 'with valid data' do
      let(:updated_product) { FactoryGirl.attributes_for(:product, title: 'updated title')}
      it 'redirects to show actions' do
        put :update, id: awesome_product, product: updated_product
        expect(response).to redirect_to(awesome_product)
      end
       it 'updates product correctly' do
         patch :update,id: awesome_product ,product: updated_product
         awesome_product.reload
         expect(awesome_product.title).to eq('updated title')

       end
    end

  end
end
