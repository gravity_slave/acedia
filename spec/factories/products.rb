FactoryGirl.define do
  factory :product do
    title "My awesome produc"
    description "My awesome description"
    image_url "myawesomepicture.jpg"
    price 1
  end
end
