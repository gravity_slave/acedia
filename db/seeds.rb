# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

10.times do |n|
 Product.create!(title: "title #{n}",
                 description: "#{n} Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean id turpis id nibh porttitor convallis.
 Sed vestibulum ullamcorper turpis ut pellentesque. Aenean dapibus sem fermentum neque laoreet, eu sodales mauris convallis.
Morbi vel volutpat velit. Cras tellus metus, suscipit id sodales non, pulvinar non tortor. Pellentesque ut magna tempus velit porttitor blandit in
id purus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.",
                 image_url: "pepetrump.png",
                 price: 322)
end

puts " 10  products have been created"

